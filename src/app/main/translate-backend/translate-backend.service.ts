import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TranslateBackendService {
  constructor(private http: HttpClient) {}

  getTranslation(text: string, language: string): Observable<any> {
    const body = {
      text: text,
      language: language
    }
    return this.http.post<any>(environment.API_URL + '/translate/', body);
  }
}
