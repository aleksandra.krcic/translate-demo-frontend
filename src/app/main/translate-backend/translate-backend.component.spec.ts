import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslateBackendComponent } from './translate-backend.component';

describe('TranslateBackendComponent', () => {
  let component: TranslateBackendComponent;
  let fixture: ComponentFixture<TranslateBackendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TranslateBackendComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TranslateBackendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
