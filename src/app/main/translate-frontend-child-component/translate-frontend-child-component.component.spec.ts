import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslateFrontendChildComponentComponent } from './translate-frontend-child-component.component';

describe('TranslateFrontendChildComponentComponent', () => {
  let component: TranslateFrontendChildComponentComponent;
  let fixture: ComponentFixture<TranslateFrontendChildComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TranslateFrontendChildComponentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TranslateFrontendChildComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
