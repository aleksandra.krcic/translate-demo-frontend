import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TranslateBackendComponent } from "./translate-backend/translate-backend.component";
import { TranslateFrontendChildComponentComponent } from "./translate-frontend-child-component/translate-frontend-child-component.component";
import { TranslateFrontendServiceComponent } from "./translate-frontend-service/translate-frontend-service.component";


const personRoutes: Routes = [
  {
    path: '',
    redirectTo: 'translate-backend',
    pathMatch: 'full'
  },
  {
    path: 'translate-backend',
    component: TranslateBackendComponent
  },
  {
    path: 'translate-frontend-child-component',
    component: TranslateFrontendChildComponentComponent
  },
  {
    path: 'translate-frontend-service',
    component: TranslateFrontendServiceComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(personRoutes) ],
  exports: [ RouterModule]
})
export class MainRoutingModule { }
