import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslateFrontendServiceComponent } from './translate-frontend-service.component';

describe('TranslateFrontendServiceComponent', () => {
  let component: TranslateFrontendServiceComponent;
  let fixture: ComponentFixture<TranslateFrontendServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TranslateFrontendServiceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TranslateFrontendServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
