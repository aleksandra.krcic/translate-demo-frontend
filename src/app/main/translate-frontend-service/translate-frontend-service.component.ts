import { Component, ViewChild } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Observable, fromEvent, withLatestFrom, distinctUntilChanged, switchMap } from 'rxjs';
import { AngularGoogleTranslationsService } from 'angular-google-translations';

@Component({
  selector: 'app-translate-frontend-service',
  templateUrl: './translate-frontend-service.component.html',
  styleUrls: ['./translate-frontend-service.component.scss']
})
export class TranslateFrontendServiceComponent {
  translated: string = "";
  translateForm = this.fb.group({
    text: ['', [Validators.required]],
    language: ['', [Validators.required]]
  });

  @ViewChild('button', {static:true}) button: any;
  clicks$: Observable<any>;
  writes$: Observable<any>;
  selects$: Observable<any>;

  languages = [
    {
      "language": "bg",
      "name": "Bulgarian"
    },
    {
      "language": "de",
      "name": "German"
    },
    {
      "language": "en",
      "name": "English"
    },
    {
      "language": "ru",
      "name": "Russian"
    },
    {
      "language": "sr",
      "name": "Serbian"
    }
  ];

  constructor(private fb: FormBuilder, public angularGoogleTranslationsService: AngularGoogleTranslationsService) { }

  ngAfterViewInit() {
    this.clicks$ = fromEvent(this.button.nativeElement, 'click');
    this.writes$ = this.translateForm.controls.text.valueChanges;
    this.selects$ = this.translateForm.controls.language.valueChanges;
    this.translateText();
  }

  translateText() {
    this.clicks$
      .pipe(
        withLatestFrom(this.writes$, this.selects$),
        distinctUntilChanged((previous: any, current: any)=> {
          // comparing text and select values
          return  previous[1] == current[1] && previous[2] == current[2];
        }),
        // switchmap for canceling previous requests with new one
        switchMap(([clickValue, textValue, selectValue]) => {
          return this.angularGoogleTranslationsService.translate(textValue, selectValue);
        })
      )
      .subscribe(translation => {
        this.translated = translation;
      })
  }
}

