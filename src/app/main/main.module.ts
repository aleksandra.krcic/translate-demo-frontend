import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainRoutingModule } from './main-routing.module';
import { TranslateBackendComponent } from './translate-backend/translate-backend.component';
import { TranslateFrontendServiceComponent } from './translate-frontend-service/translate-frontend-service.component';
import { TranslateFrontendChildComponentComponent } from './translate-frontend-child-component/translate-frontend-child-component.component';
import { AngularGoogleTranslationsModule } from 'angular-google-translations';

@NgModule({
  declarations: [
    TranslateBackendComponent,
    TranslateFrontendServiceComponent,
    TranslateFrontendChildComponentComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularGoogleTranslationsModule
  ]
})
export class MainModule { }
